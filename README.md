## Advanced Software Engineering
### Piccolo App Jan Hansel

### Instructions
Benötigt wird Android Studio \>=4.1.
Mit der Installation sollte auch das Android SDK und Java SDK installiert sein.

Anschließend kann durch den Start des Moduls app die App im Simulator getestet werden.

Durch das Ausführen von All Tests können alle vorhandenen Unittests ausgeführt werden.
