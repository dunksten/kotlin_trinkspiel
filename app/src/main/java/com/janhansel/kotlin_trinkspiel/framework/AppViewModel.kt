package com.janhansel.kotlin_trinkspiel.framework

import android.app.Application
import androidx.lifecycle.AndroidViewModel

open class AppViewModel(application: Application, protected val interactors: Interactors) :
    AndroidViewModel(application)