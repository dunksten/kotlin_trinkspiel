package com.janhansel.kotlin_trinkspiel.framework.dao

import androidx.room.*
import com.janhansel.kotlin_trinkspiel.framework.entity.PlayerEntity

@Dao
interface PlayerDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPlayer(playerEntity: PlayerEntity): Long

    @Delete
    suspend fun deletePlayer(playerEntity: PlayerEntity)

    @Query("DELETE FROM players")
    suspend fun deleteAllPlayers()

    @Query("SELECT * FROM players ORDER BY name")
    suspend fun getAllPlayers(): List<PlayerEntity>

    @Query("SELECT * FROM players WHERE id = :id")
    suspend fun getPlayerById(id: Long): PlayerEntity?
}