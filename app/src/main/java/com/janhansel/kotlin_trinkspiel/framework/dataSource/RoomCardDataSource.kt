package com.janhansel.kotlin_trinkspiel.framework.dataSource

import android.content.Context
import com.janhansel.kotlin_trinkspiel.data.CardDataSource
import com.janhansel.kotlin_trinkspiel.domain.model.Card
import com.janhansel.kotlin_trinkspiel.domain.valueobject.Score
import com.janhansel.kotlin_trinkspiel.framework.db.AppDatabase
import com.janhansel.kotlin_trinkspiel.framework.entity.CardEntity


class RoomCardDataSource(context: Context) : CardDataSource {

    private val cardDao = AppDatabase.getInstance(context).cardDao()

    override suspend fun insertCard(card: Card): Long {
        return cardDao.insertCard(
            CardEntity(
                id = card.id,
                text = card.text,
                score = card.score.score
            )
        )
    }

    override suspend fun deleteCard(card: Card) =
        cardDao.deleteCard(CardEntity(id = card.id, text = card.text, score = card.score.score))

    override suspend fun deleteAllCards() = cardDao.deleteAllCards()

    override suspend fun getAllCards(): List<Card> =
        cardDao.getAllCards().map { Card(it.id, it.text, Score(it.score)) }

    override suspend fun getCard(id: Long): Card? {
        return cardDao.getCardById(id).let {
            it?.let { it1 ->
                Card(
                    it1.id, it.text,
                    Score(it.score)
                )
            }
        }
    }
}