package com.janhansel.kotlin_trinkspiel.framework.dao

import androidx.room.*
import com.janhansel.kotlin_trinkspiel.framework.entity.CardEntity

@Dao
interface CardDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCard(cardEntity: CardEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllCards(cardEntities: List<CardEntity>)

    @Delete
    suspend fun deleteCard(cardEntity: CardEntity)

    @Query("SELECT * FROM cards ORDER BY score")
    suspend fun getAllCards(): List<CardEntity>

    @Query("SELECT * FROM cards WHERE id = :id")
    suspend fun getCardById(id: Long): CardEntity?

    @Query("DELETE FROM cards")
    suspend fun deleteAllCards()
}