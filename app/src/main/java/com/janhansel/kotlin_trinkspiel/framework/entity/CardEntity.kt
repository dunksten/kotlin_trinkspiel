package com.janhansel.kotlin_trinkspiel.framework.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(tableName = "cards")
@Parcelize
data class CardEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L,
    @ColumnInfo(name = "text")
    var text: String,
    @ColumnInfo(name = "score")
    var score: Int
) : Parcelable