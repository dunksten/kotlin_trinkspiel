package com.janhansel.kotlin_trinkspiel.framework.dataSource

import android.content.Context
import com.janhansel.kotlin_trinkspiel.data.PlayerDataSource
import com.janhansel.kotlin_trinkspiel.domain.model.Player
import com.janhansel.kotlin_trinkspiel.domain.valueobject.Score
import com.janhansel.kotlin_trinkspiel.framework.db.AppDatabase
import com.janhansel.kotlin_trinkspiel.framework.entity.PlayerEntity


class RoomPlayerDataSource(context: Context) : PlayerDataSource {

    private val playerDao = AppDatabase.getInstance(context).playerDao()

    override suspend fun insertPlayer(player: Player): Long {
        return playerDao.insertPlayer(PlayerEntity(name = player.name, score = player.score.score))
    }

    override suspend fun deletePlayer(player: Player) = playerDao.deletePlayer(
        PlayerEntity(
            id = player.id,
            name = player.name,
            score = player.score.score
        )
    )

    override suspend fun deleteAllPlayers() = playerDao.deleteAllPlayers()

    override suspend fun getAllPlayers(): List<Player> =
        playerDao.getAllPlayers().map { Player(it.id, it.name, Score(it.score)) }

    override suspend fun getPlayer(id: Long): Player? =
        playerDao.getPlayerById(id).let {
            it?.let { it1 ->
                Player(
                    it1.id, it.name,
                    Score(it.score)
                )
            }
        }
}