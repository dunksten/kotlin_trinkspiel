package com.janhansel.kotlin_trinkspiel.framework.db

import android.content.Context
import android.content.res.Resources
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.janhansel.kotlin_trinkspiel.R
import com.janhansel.kotlin_trinkspiel.framework.dao.CardDao
import com.janhansel.kotlin_trinkspiel.framework.dao.PlayerDao
import com.janhansel.kotlin_trinkspiel.framework.entity.CardEntity
import com.janhansel.kotlin_trinkspiel.framework.entity.PlayerEntity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

@Database(
    entities = [(PlayerEntity::class), (CardEntity::class)],
    version = 3,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun playerDao(): PlayerDao
    abstract fun cardDao(): CardDao


    private class DatabaseCallback(
        private val resources: Resources
    ) : RoomDatabase.Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let { database ->
                GlobalScope.launch {
                    val cardDao = database.cardDao()
                    preLoad(cardDao)
                }
            }
        }

        private suspend fun preLoad(cardDao: CardDao) {
            // 1
            val jsonString = resources.openRawResource(R.raw.cards).bufferedReader().use {
                it.readText()
            }
            // 2
            val typeToken = object : TypeToken<List<CardEntity>>() {}.type
            val cards = Gson().fromJson<List<CardEntity>>(jsonString, typeToken)
            // 3
            cardDao.insertAllCards(cards)
        }
    }

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "app_database"
                )
                    .addCallback(DatabaseCallback(context.resources))
                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}