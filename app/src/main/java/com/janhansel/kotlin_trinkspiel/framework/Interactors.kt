package com.janhansel.kotlin_trinkspiel.framework

import com.janhansel.kotlin_trinkspiel.interactors.*

data class Interactors(
    val addPlayer: AddPlayer,
    val deletePlayer: DeletePlayer,
    val deletePlayers: DeletePlayers,
    val getPlayers: GetPlayers,
    val addCard: AddCard,
    val deleteCard: DeleteCard,
    val getCards: GetCards
)
