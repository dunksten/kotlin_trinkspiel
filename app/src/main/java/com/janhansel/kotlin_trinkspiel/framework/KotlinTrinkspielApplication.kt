package com.janhansel.kotlin_trinkspiel.framework

import android.app.Application
import com.janhansel.kotlin_trinkspiel.data.CardRepository
import com.janhansel.kotlin_trinkspiel.data.PlayerRepository
import com.janhansel.kotlin_trinkspiel.framework.dataSource.RoomCardDataSource
import com.janhansel.kotlin_trinkspiel.framework.dataSource.RoomPlayerDataSource
import com.janhansel.kotlin_trinkspiel.interactors.*


class KotlinTrinkspielApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        this.deleteDatabase("app_database")

        val playerRepository = PlayerRepository(RoomPlayerDataSource(this))
        val cardRepository = CardRepository(RoomCardDataSource(this))

        AppViewModelFactory.inject(
            this,
            Interactors(
                AddPlayer(playerRepository),
                DeletePlayer(playerRepository),
                DeletePlayers(playerRepository),
                GetPlayers(playerRepository),
                AddCard(cardRepository),
                DeleteCard(cardRepository),
                GetCards(cardRepository)
            )
        )
    }
}