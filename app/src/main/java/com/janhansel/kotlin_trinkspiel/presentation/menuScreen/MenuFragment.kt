package com.janhansel.kotlin_trinkspiel.presentation.menuScreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.janhansel.kotlin_trinkspiel.databinding.FragmentMenuBinding
import com.janhansel.kotlin_trinkspiel.framework.NavigationHost
import com.janhansel.kotlin_trinkspiel.presentation.playerScreen.PlayerInputFragment

class MenuFragment : Fragment() {
    private var _binding: FragmentMenuBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentMenuBinding.inflate(inflater, container, false)
        val view = binding.root
        binding.button.setOnClickListener {
            (activity as NavigationHost).navigateTo(PlayerInputFragment(), true)
        }
        return view
    }
}