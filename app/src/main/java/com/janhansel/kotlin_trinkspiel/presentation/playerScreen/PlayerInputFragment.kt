package com.janhansel.kotlin_trinkspiel.presentation.playerScreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.janhansel.kotlin_trinkspiel.databinding.FragmentPlayerInputBinding
import com.janhansel.kotlin_trinkspiel.framework.AppViewModelFactory
import com.janhansel.kotlin_trinkspiel.framework.NavigationHost
import com.janhansel.kotlin_trinkspiel.presentation.gameScreen.ShowCardFragment


class PlayerInputFragment : Fragment() {

    private lateinit var playerViewModel: PlayerViewModel
    private lateinit var adapter: PlayerAdapter

    private var enoughPlayers: Boolean = false

    private var _binding: FragmentPlayerInputBinding? = null
    private val binding
        get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentPlayerInputBinding.inflate(inflater, container, false)

        playerViewModel =
            ViewModelProviders.of(this, AppViewModelFactory).get(PlayerViewModel::class.java)

        adapter = PlayerAdapter(mutableListOf())
        adapter.setOnPlayerTapListener { player ->
            playerViewModel.deletePlayer(player)
        }
        // Set bindings
        setBindings(adapter)
        //Set Observer
        playerViewModel.playerList.observe(viewLifecycleOwner, { players ->
            adapter.setItems(players)
            enoughPlayers = players.size > 2
        })
        return binding.root
    }

    private fun setBindings(adapter: PlayerAdapter) {
        binding.recyclerview.adapter = adapter
        binding.button2.setOnClickListener { onNewPlayer() }
        binding.button4.setOnClickListener {
            if (enoughPlayers) (activity as NavigationHost).navigateTo(ShowCardFragment(), true)
        }
    }

    private fun onNewPlayer() {
        playerViewModel.onNewPlayer(binding.textInputEditText.text.toString())
        binding.textInputEditText.setText("")
    }

}