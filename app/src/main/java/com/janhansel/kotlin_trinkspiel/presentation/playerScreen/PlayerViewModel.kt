package com.janhansel.kotlin_trinkspiel.presentation.playerScreen

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.janhansel.kotlin_trinkspiel.domain.model.Player
import com.janhansel.kotlin_trinkspiel.domain.valueobject.Score
import com.janhansel.kotlin_trinkspiel.framework.AppViewModel
import com.janhansel.kotlin_trinkspiel.framework.Interactors
import kotlinx.coroutines.launch

class PlayerViewModel(
    application: Application, interactors: Interactors
) : AppViewModel(application, interactors) {

    var playerList: MutableLiveData<List<Player>> = MutableLiveData()

    init {
        viewModelScope.launch { interactors.deletePlayers(); getAllPlayers() }
        Log.i("PlayerViewModel", "PlayerViewModel created!")
    }

    fun onNewPlayer(playerName: String) {
        viewModelScope.launch {
            if (playerName.isNotBlank()) {
                val newPlayer = Player(0, playerName, Score())
                insert(newPlayer)
                getAllPlayers()
            }
        }
    }

    private suspend fun insert(player: Player) {
        interactors.addPlayer(player)
    }

    fun deletePlayer(player: Player) = viewModelScope.launch {
        interactors.deletePlayer(player)
        getAllPlayers()
    }

    private suspend fun getAllPlayers() {
        playerList.postValue(interactors.getPlayers())
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("PlayerViewModel", "PlayerViewModel destroyed!")
    }

}