package com.janhansel.kotlin_trinkspiel.presentation.gameScreen

import android.app.Application
import android.util.Log
import androidx.lifecycle.viewModelScope
import com.janhansel.kotlin_trinkspiel.domain.model.Card
import com.janhansel.kotlin_trinkspiel.domain.model.Player
import com.janhansel.kotlin_trinkspiel.framework.AppViewModel
import com.janhansel.kotlin_trinkspiel.framework.Interactors
import kotlinx.coroutines.launch

class CardViewModel(
    application: Application, interactors: Interactors
) : AppViewModel(application, interactors) {

    private var cards: MutableList<Card> = mutableListOf()
    private var players: List<Player> = listOf()

    private var playerCount: Int = 0
    private var currentPlayerIndex = 0

    init {
        Log.i("CardViewModel", "CardViewModel created!")
        viewModelScope.launch { getCards(); getPlayers() }
    }

    private suspend fun getCards() {
        cards = interactors.getCards().toMutableList()
        cards.shuffled()
    }


    private suspend fun getPlayers() {
        players = interactors.getPlayers()
        playerCount = players.size - 1
    }

    suspend fun onNextCard(): Card? {
        return if (cards.isNotEmpty()) {
            val card: Card = cards.first()
            cards.removeFirst()
            return card
        } else {
            getCards()
            onNextCard()
        }
    }

    suspend fun onNextPlayer(): Player {
        if (currentPlayerIndex < playerCount) {
            currentPlayerIndex++
        } else {
            currentPlayerIndex = 0
        }
        return if (players.isNotEmpty()) {
            return players[currentPlayerIndex]
        } else {
            getPlayers()
            onNextPlayer()
        }

    }

    override fun onCleared() {
        super.onCleared()
        Log.i("CardViewModel", "CardViewModel destroyed!")
    }

}