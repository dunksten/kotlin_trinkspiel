package com.janhansel.kotlin_trinkspiel.presentation.gameScreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.lifecycleScope
import com.janhansel.kotlin_trinkspiel.databinding.FragmentShowCardBinding
import com.janhansel.kotlin_trinkspiel.framework.AppViewModelFactory
import kotlinx.coroutines.launch

class ShowCardFragment : Fragment() {
    private lateinit var cardViewModel: CardViewModel
    private var _binding: FragmentShowCardBinding? = null
    private val binding get() = _binding!!

    private lateinit var cardView: TextView
    private lateinit var playerView: TextView



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentShowCardBinding.inflate(inflater, container, false)

        // View Model
        cardViewModel =
            ViewModelProviders.of(this, AppViewModelFactory).get(CardViewModel::class.java)

        // Bindings
        setBindings()

        // First card
        lifecycleScope.launch {
            onNextButton()
        }
        return binding.root
    }

    private fun setBindings() {
        cardView = binding.cardView
        playerView = binding.playerView
        binding.nextCardButton.setOnClickListener {
            lifecycleScope.launch {
                onNextButton()
            }
        }
    }

    private suspend fun onNextButton() {
        cardView.text = cardViewModel.onNextCard()?.text
        playerView.text = cardViewModel.onNextPlayer().name
    }
}