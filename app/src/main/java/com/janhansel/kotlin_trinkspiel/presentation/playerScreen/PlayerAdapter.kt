package com.janhansel.kotlin_trinkspiel.presentation.playerScreen

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.janhansel.kotlin_trinkspiel.databinding.RecyclerviewItemRowBinding
import com.janhansel.kotlin_trinkspiel.domain.model.Player

class PlayerAdapter(private var players: MutableList<Player>) :
    RecyclerView.Adapter<PlayerAdapter.PlayerHolder>() {

    private var listener: ((Player) -> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PlayerHolder {
        // Inflate the layout for this fragment
        val inflater = LayoutInflater.from(parent.context)
        val binding = RecyclerviewItemRowBinding.inflate(inflater, parent, false)
        val view = binding.root
        return PlayerHolder(view)
    }

    override fun onBindViewHolder(holder: PlayerHolder, position: Int) {
        val player: Player = players[position]
        holder.playerName.text = player.name

        holder.deleteButton.setOnClickListener {
            listener?.invoke(players[position])
        }
    }

    override fun getItemCount(): Int = players.size

    fun setOnPlayerTapListener(listener: ((Player) -> Unit)) {
        this.listener = listener
    }

    fun setItems(players: List<Player>) {
        this.players.clear()
        this.players.addAll(players)
        Log.i("PlayerAdapter", this.players.toString())
        notifyDataSetChanged()
    }

    class PlayerHolder(view: View) :
        RecyclerView.ViewHolder(view) {
        private val binding = RecyclerviewItemRowBinding.bind(view)
        val playerName: TextView = binding.editText
        val deleteButton: Button = binding.button3
    }
}
