package com.janhansel.kotlin_trinkspiel

import android.content.Context
import android.util.Log
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.janhansel.kotlin_trinkspiel.domain.model.Player
import com.janhansel.kotlin_trinkspiel.domain.valueobject.Score
import com.janhansel.kotlin_trinkspiel.framework.dataSource.RoomPlayerDataSource
import com.janhansel.kotlin_trinkspiel.framework.db.AppDatabase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RoomPlayerDataSourceTest {

    private lateinit var database: AppDatabase
    private lateinit var roomPlayerDataSource: RoomPlayerDataSource

    @Before
    fun setup() {
        val context: Context = InstrumentationRegistry.getInstrumentation().targetContext
        try {
            database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java)
                .build()
        } catch (e: Exception) {
            e.message?.let { Log.i("test", it) }
        }
        roomPlayerDataSource = RoomPlayerDataSource(context)

    }

    @Test
    fun testDeletePlayers() = runBlocking {
        roomPlayerDataSource.deleteAllPlayers()

        val emptyList: List<Player> = emptyList()
        val retrievedPlayers = roomPlayerDataSource.getAllPlayers()

        Assert.assertEquals(emptyList, retrievedPlayers)
    }

    @Test
    fun testAddPlayer() = runBlocking {
        roomPlayerDataSource.deleteAllPlayers()

        val player = Player(0L, "Test", score = Score())

        val retrievedPlayer = roomPlayerDataSource.getPlayer(roomPlayerDataSource.insertPlayer(player))

        Assert.assertEquals("Test", retrievedPlayer?.name)
    }

    @Test
    fun testDeletePlayer() = runBlocking {
        roomPlayerDataSource.deleteAllPlayers()

        val player = Player(0, "Test1", score = Score())

        val retrievedPlayer = roomPlayerDataSource.getPlayer(roomPlayerDataSource.insertPlayer(player))
        if (retrievedPlayer != null) {
            roomPlayerDataSource.deletePlayer(retrievedPlayer)
        }

        val postDeletePlayer = roomPlayerDataSource.getAllPlayers()
        Assert.assertTrue(postDeletePlayer.isEmpty())
    }

    @After
    fun tearDown() {
        database.close()
    }
}