package com.janhansel.kotlin_trinkspiel

import android.content.Context
import android.util.Log
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.janhansel.kotlin_trinkspiel.domain.model.Card
import com.janhansel.kotlin_trinkspiel.domain.valueobject.Score
import com.janhansel.kotlin_trinkspiel.framework.dataSource.RoomCardDataSource
import com.janhansel.kotlin_trinkspiel.framework.db.AppDatabase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RoomCardDataSourceTest {

    private lateinit var database: AppDatabase
    private lateinit var roomCardDataSource: RoomCardDataSource

    @Before
    fun setup() {
        val context: Context = InstrumentationRegistry.getInstrumentation().targetContext
        try {
            database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java)
                .build()
        } catch (e: Exception) {
            e.message?.let { Log.i("test", it) }
        }
        roomCardDataSource = RoomCardDataSource(context)

    }

    @Test
    fun testDeleteCards() = runBlocking {
        roomCardDataSource.deleteAllCards()

        val emptyList: List<Card> = emptyList()
        val retrievedCards = roomCardDataSource.getAllCards()

        Assert.assertEquals(emptyList, retrievedCards)
    }

    @Test
    fun testAddCard() = runBlocking {
        roomCardDataSource.deleteAllCards()

        val card = Card(0L, "Test", score = Score())

        val retrievedCard = roomCardDataSource.getCard(roomCardDataSource.insertCard(card))

        Assert.assertEquals("Test", retrievedCard?.text)
    }

    @Test
    fun testDeleteCard() = runBlocking {
        roomCardDataSource.deleteAllCards()
        val card = Card(0L, "Test1", score = Score())

        val retrievedCard = roomCardDataSource.getCard(roomCardDataSource.insertCard(card))
        if (retrievedCard != null) {
            roomCardDataSource.deleteCard(retrievedCard)
        }

        val postDeleteCard = roomCardDataSource.getAllCards()
        Assert.assertTrue(postDeleteCard.isEmpty())
    }

    @After
    fun tearDown() {
        database.close()
    }
}