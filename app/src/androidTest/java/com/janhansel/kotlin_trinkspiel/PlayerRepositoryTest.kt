package com.janhansel.kotlin_trinkspiel

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.janhansel.kotlin_trinkspiel.data.PlayerRepository
import com.janhansel.kotlin_trinkspiel.domain.model.Player
import com.janhansel.kotlin_trinkspiel.domain.valueobject.Score
import com.janhansel.kotlin_trinkspiel.framework.dataSource.RoomPlayerDataSource
import io.mockk.coEvery
import io.mockk.mockkClass
import io.mockk.unmockkAll
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PlayerRepositoryTest {

    private lateinit var playerRepository: PlayerRepository
    private lateinit var player: Player

    private var roomPlayerDataSource = mockkClass(RoomPlayerDataSource::class)

    @Before
    fun setup() {
        playerRepository = PlayerRepository(roomPlayerDataSource)
        player = Player(0L, "Test", score = Score())
        coEvery { roomPlayerDataSource.insertPlayer(player) } returns 0L
        coEvery { roomPlayerDataSource.getPlayer(0L) } returns player
        coEvery { roomPlayerDataSource.deletePlayer(player) } returns Unit
        coEvery { roomPlayerDataSource.getAllPlayers() } returns emptyList()
    }

    @Test
    fun testAddPlayer() = runBlocking {
        val id = playerRepository.insertPlayer(player)

        val retrievedPlayer = playerRepository.getPlayer(id)

        Assert.assertEquals(player, retrievedPlayer)
    }

    @Test
    fun testDeletePlayer() = runBlocking {
        val retrievedPlayer = playerRepository.getPlayer(0L)
        if (retrievedPlayer != null) {
            playerRepository.deletePlayer(retrievedPlayer)
        }
        val deletedPlayer = playerRepository.getAllPlayers()
        Assert.assertTrue(deletedPlayer.isEmpty())
    }

    @After
    fun tearDown() {
        unmockkAll()
    }
}