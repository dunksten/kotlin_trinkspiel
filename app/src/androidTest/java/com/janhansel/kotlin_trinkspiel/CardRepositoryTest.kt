package com.janhansel.kotlin_trinkspiel

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.janhansel.kotlin_trinkspiel.data.CardRepository
import com.janhansel.kotlin_trinkspiel.domain.model.Card
import com.janhansel.kotlin_trinkspiel.domain.valueobject.Score
import com.janhansel.kotlin_trinkspiel.framework.dataSource.RoomCardDataSource
import io.mockk.coEvery
import io.mockk.mockkClass
import io.mockk.unmockkAll
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CardRepositoryTest {

    private lateinit var cardRepository: CardRepository
    private lateinit var card: Card

    private var roomCardDataSource = mockkClass(RoomCardDataSource::class)

    @Before
    fun setup() {
        cardRepository = CardRepository(roomCardDataSource)
        card = Card(0L, "Test", score = Score())
        coEvery { roomCardDataSource.insertCard(card) } returns 0L
        coEvery { roomCardDataSource.getCard(0L) } returns card
        coEvery { roomCardDataSource.deleteCard(card) } returns Unit
        coEvery { roomCardDataSource.getAllCards() } returns emptyList()
    }

    @Test
    fun testAddCard() = runBlocking {
        val id = cardRepository.insertCard(card)

        val retrievedCard = cardRepository.getCard(id)

        Assert.assertEquals(card, retrievedCard)
    }

    @Test
    fun testDeleteCard() = runBlocking {
        val retrievedCard = cardRepository.getCard(0L)
        if (retrievedCard != null) {
            cardRepository.deleteCard(retrievedCard)
        }
        val deletedCard = cardRepository.getAllCards()
        Assert.assertTrue(deletedCard.isEmpty())
    }

    @After
    fun tearDown() {
        unmockkAll()
    }
}