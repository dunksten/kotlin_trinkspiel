package com.janhansel.kotlin_trinkspiel.data

import com.janhansel.kotlin_trinkspiel.domain.model.Card

interface CardDataSource {

    suspend fun insertCard(card: Card): Long

    suspend fun deleteCard(card: Card)

    suspend fun deleteAllCards()

    suspend fun getAllCards(): List<Card>

    suspend fun getCard(id: Long): Card?
}