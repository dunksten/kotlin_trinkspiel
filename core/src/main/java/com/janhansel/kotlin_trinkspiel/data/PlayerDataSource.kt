package com.janhansel.kotlin_trinkspiel.data

import com.janhansel.kotlin_trinkspiel.domain.model.Player

interface PlayerDataSource {

    suspend fun insertPlayer(player: Player): Long

    suspend fun deletePlayer(player: Player)

    suspend fun deleteAllPlayers()

    suspend fun getAllPlayers(): List<Player>

    suspend fun getPlayer(id: Long): Player?
}