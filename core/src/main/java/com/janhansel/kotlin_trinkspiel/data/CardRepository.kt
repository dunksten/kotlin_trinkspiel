package com.janhansel.kotlin_trinkspiel.data

import com.janhansel.kotlin_trinkspiel.domain.model.Card

class CardRepository(private val dataSource: CardDataSource) {
    suspend fun insertCard(card: Card): Long = dataSource.insertCard(card)
    suspend fun getAllCards() = dataSource.getAllCards()
    suspend fun getCard(id: Long) = dataSource.getCard(id)
    suspend fun deleteCard(card: Card) = dataSource.deleteCard(card)
    suspend fun deleteCards() = dataSource.deleteAllCards()
}