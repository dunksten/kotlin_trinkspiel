package com.janhansel.kotlin_trinkspiel.data

import com.janhansel.kotlin_trinkspiel.domain.model.Player

class PlayerRepository(private val dataSource: PlayerDataSource) {
    suspend fun insertPlayer(player: Player): Long = dataSource.insertPlayer(player)
    suspend fun getAllPlayers() = dataSource.getAllPlayers()
    suspend fun getPlayer(id: Long) = dataSource.getPlayer(id)
    suspend fun deletePlayer(player: Player) = dataSource.deletePlayer(player)
    suspend fun deletePlayers() = dataSource.deleteAllPlayers()
}