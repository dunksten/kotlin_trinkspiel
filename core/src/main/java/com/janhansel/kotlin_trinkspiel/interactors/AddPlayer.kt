package com.janhansel.kotlin_trinkspiel.interactors

import com.janhansel.kotlin_trinkspiel.data.PlayerRepository
import com.janhansel.kotlin_trinkspiel.domain.model.Player

class AddPlayer(private val playerRepository: PlayerRepository) {
    suspend operator fun invoke(player: Player) = playerRepository.insertPlayer(player)
}