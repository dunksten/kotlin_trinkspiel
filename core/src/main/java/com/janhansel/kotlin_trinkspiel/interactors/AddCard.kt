package com.janhansel.kotlin_trinkspiel.interactors

import com.janhansel.kotlin_trinkspiel.data.CardRepository
import com.janhansel.kotlin_trinkspiel.domain.model.Card

class AddCard(private val cardRepository: CardRepository) {
    suspend operator fun invoke(card: Card) = cardRepository.insertCard(card)
}