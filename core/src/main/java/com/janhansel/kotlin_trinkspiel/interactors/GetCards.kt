package com.janhansel.kotlin_trinkspiel.interactors

import com.janhansel.kotlin_trinkspiel.data.CardRepository

class GetCards(private val cardRepository: CardRepository) {
    suspend operator fun invoke() = cardRepository.getAllCards()
}