package com.janhansel.kotlin_trinkspiel.interactors

import com.janhansel.kotlin_trinkspiel.data.CardRepository
import com.janhansel.kotlin_trinkspiel.domain.model.Card

class DeleteCard(private val cardRepository: CardRepository) {
    suspend operator fun invoke(card: Card) = cardRepository.deleteCard(card)
}