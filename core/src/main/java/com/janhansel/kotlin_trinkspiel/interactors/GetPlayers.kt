package com.janhansel.kotlin_trinkspiel.interactors

import com.janhansel.kotlin_trinkspiel.data.PlayerRepository

class GetPlayers(private val playerRepository: PlayerRepository) {
    suspend operator fun invoke() = playerRepository.getAllPlayers()
}