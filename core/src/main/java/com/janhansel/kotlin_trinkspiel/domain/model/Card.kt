package com.janhansel.kotlin_trinkspiel.domain.model

import com.janhansel.kotlin_trinkspiel.domain.valueobject.Score

data class Card(
    val id: Long = 0L,
    val text: String,
    val score: Score
)
