package com.janhansel.kotlin_trinkspiel.domain.model

import com.janhansel.kotlin_trinkspiel.domain.valueobject.Score

data class Player(
    val id: Long = 0L,
    val name: String,
    val score: Score
)