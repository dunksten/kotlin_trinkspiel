package com.janhansel.kotlin_trinkspiel.domain.valueobject

data class Score(val score: Int = 0)
