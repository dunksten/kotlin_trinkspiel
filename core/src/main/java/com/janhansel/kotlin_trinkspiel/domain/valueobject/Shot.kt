package com.janhansel.kotlin_trinkspiel.domain.valueobject

data class Shot(val amount: Int = 0)
